#! /usr/bin/bash
with_gui () {
  /usr/bin/stow alacritty
  /usr/bin/stow kakoune
  /usr/bin/stow zsh
}
with_all () {
  /usr/bin/stow git
}

WORKDIR=/home/$(id -un 1000)/dotfiles
cd $WORKDIR
#/usr/bin/git pull

with_gui;
with_all;
