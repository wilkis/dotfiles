# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="gianu"

plugins=(
  git
  history
  last-working-dir
  common-aliases
  fzf
)

source $ZSH/oh-my-zsh.sh

alias digs="dig +short"
alias sshlive="ssh -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' -o 'GlobalKnownHostsFile /dev/null'"

export VISUAL=kak
export EDITOR="$VISUAL"

if [ -n "${commands[fzf-share]}" ]; then
  source "$(fzf-share)/key-bindings.zsh"
fi
