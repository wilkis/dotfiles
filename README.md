My .dotfiles repo
=================

# How it works

## In short
* `sudo ~/dotfiles/stowoutpull.sh`

# Applications

## [Alacritty](https://github.com/alacritty/alacritty)
* Minimal terminal emulator
* With Control+C copy support😅

## git
* My gitconfig

## [zsh](https://www.zsh.org/)
* Unix shell

# License
* licensed unter [MIT](LICENSE)
